@component('mail::message')


<h4>Hi, <strong>{{ $data['username'] }}!</strong></h4>
	Please click the link to change your acccount password
	Email: {{$data['useremail']}} 
    Password:{{ $data['userpassword'] }}

@component('mail::button', ['url' => 'http://localhost/TSS/validatepassword/'.$data['userslug']])
Change Password
@endcomponent


Thanks,<br>
{{ config('app.name') }}
<img src="{{url('uploads/fevicon/'.$onefevicon->image)}}" width="100" height="100">
@endcomponent
