<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use App\Portfolio;
use Carbon\Carbon;

class PortfolioController extends Controller
{
   public function createportfolio(Request $request)
    {
        
        return view('cd-admin.portfolio.portfolioinsert');
    }


    public function storeportfolio(Request $request)


    {
    	
    	$validatedData = $request->validate([
			'title'  => 'required',
			'date'  => 'required',
			'name'  => 'required',
			'category'  => 'required',
			'status'  => 'required',
			'image.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'


		]);
        
           $fdes = new Portfolio;
            $fdes->title= $request->title;
            
         $test = str_replace('/', '-', $request->date);
          $newDate = date("Y-m-d", strtotime($test));
         $fdes->date = $newDate;
        
          if($request->hasFile('image'))
        {
            $file=Input::file('image');
            $destinationpath='uploads/portfolio';
            $uploadedImageName = uniqid().'_'.$file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath,$uploadedImageName);
            $fdes->image=$uploadedImageName;
        }
        $fdes->name= $request->name;
        $fdes->link= $request->link;
        $fdes->category= $request->category;
        $fdes->slogan= $request->slogan;
        $fdes->status= $request->status;
        $fdes->alt_image=$request->title;
        $fdes->description=$request->description;
        $fdes->save();
       

        session()->flash('status','insert');
        return redirect('/cd-admin/viewportfolio');
 
    }


    public function viewportfolio(Request $request)
    {
        
        $portfolio = Portfolio::all();
        return view('cd-admin.portfolio.portfolioview',['portfolio'=>$portfolio]);
    }


    public function changeStatus($id)
     {
        try {
            $id = (int)$id;
            $team = Portfolio::find($id);
                if ($team->status == 0){
                    $team->status = 1;
                    $team->save();
                    session()->flash('status','change');
                    return back();
                }else{
                $team->status = 0;
                $team->save();
               session()->flash('status','change');
                return back();
            }
            
        } catch (\Exception $e) {
            $message = $e->getMessage();
            session()->flash('error', $message);
            return back();
        }

    }


    public function  editportfolio(Request $request,$id){
        $getedits = Portfolio::where('id',$id)->get()->first();
        return view('cd-admin.portfolio.portfolioedit',['getedits'=>$getedits]);
    }




    public function updateportfolio(Request $request,$id)


    {
        

        $validatedData = $request->validate([
          'title'  => 'required',
            'date'  => 'required',
            'name'  => 'required',
            'category'  => 'required',
            'status'  => 'required',
            'image.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'


        ]);
        
           $getedits = Portfolio::findorfail($id);
          $getedits->title= $request->title;
            
         $test = str_replace('/', '-', $request->date);
          $newDate = date("Y-m-d", strtotime($test));
         $getedits->date = $newDate;
          if($request->hasFile('image'))
        {

             $data_image = portfolio::findOrFail($id);
            if ($data_image->image && file_exists("uploads/portfolio/{$data_image->image}")) {
                unlink("uploads/portfolio/{$data_image->image}");
            }
            $file=Input::file('image');
            $destinationpath='uploads/portfolio';
            $uploadedImageName = uniqid().'_'.$file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath,$uploadedImageName);
            $getedits->image=$uploadedImageName;
        }
          $getedits->name= $request->name;
        $getedits->link= $request->link;
        $getedits->category= $request->category;
        $getedits->slogan= $request->slogan;
        $getedits->status= $request->status;
        $getedits->alt_image=$request->title;
        $getedits->description=$request->description;
        $getedits->update();
       

        session()->flash('status','success');
        return redirect('/cd-admin/viewportfolio');
 
    }
    


    public function deleteportfolio($id) {
        // delete carousel image from folder
        $team_image = Portfolio::findOrFail($id);
        if ($team_image->image && file_exists("uploads/portfolio/{$team_image->image}")) {
            unlink("uploads/portfolio/{$team_image->image}");
        }
        $portfolio = Portfolio::destroy($id);
        Session::flash('status','delet');
        return redirect('/cd-admin/viewportfolio');
    }
}
