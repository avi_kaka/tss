@extends('cd-admin.home-master')

@section('page-title')
Service Insert
@endsection


@section('content')


<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Service Insert</div>
            
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{url('/cd-admin/storeservice')}}" method="post" class="form-horizontal"  enctype="multipart/form-data">
                @csrf
                <div class="form-body">

                    <div class="form-group">
                            <label class="col-md-3 control-label"> Title</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa fa-user"></i>
                                    <input type="text" class="form-control input-circle" placeholder=" Title" name="title" value="{{ old('title') }}" > </div>
                                    @if ($errors->has('title'))
                                                          <span class="help-block">{{ $errors->first('title') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>

                            <div class="form-group">
                            <label class="col-md-3 control-label"> Icon</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa ab fa-facebook-f"></i>
                                    <input type="text" class="form-control input-circle" placeholder=" fa-laptop" name="icon" value="{{ old('icon') }}" > </div>
                                    @if ($errors->has('icon'))
                                                          <span class="help-block">{{ $errors->first('icon') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>

                        <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}   margin-top-20">
                        <label class="control-label col-md-3"> shotr Description

                        </label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                 <textarea   name="description">{{old('description')}}</textarea>
                                @if($errors->has('description'))
                                <span class="text-danger font-weight-danger">
                                    {{$errors->first('description')}}
                                </span>

                                @endif
                            </div>
                        </div>
                    </div>

                                   






                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="inputUserType3" class="col-md-3 control-label">Active</label>
                                <div class="col-md-4">
                                     <div class="md-radio">
                                    <input type="radio" id="radio14" name="status" value="1" class="md-radiobtn">
                                    <label for="radio14">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> Yes </label>
                                    </div>
                                     <div class="md-radio has-error">
                                        <input type="radio" id="radio15" name="status" value="0"  class="md-radiobtn">
                                        <label for="radio15">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> NO </label>
                                        </div>
                                        @if ($errors->has('status'))
                                                          <span class="help-block">{{ $errors->first('status') }}</span>
                                                        @endif

                                </div>
                            </div>
                        
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>

                    <script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
                     <script >
                         tinymce.init({
                            selector:'textarea'
                         })
                     </script>


                    @endsection