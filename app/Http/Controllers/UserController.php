<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Session;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\support\facades\Input;
use Illuminate\Contracts\Auth\Guard;
use \Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\User;
use App\Mail\Validation;
use App\Mail\PasswordFormMail;
use Hash;
use Mail;
use Auth;

class UserController extends Controller
{public function createuser(Request $request)
    {
        if( Auth::user()->status == 'super-admin'){
        return view('cd-admin.user.userinsert');
    }
        else{
        	return redirect('/cd-admin/home');

        }
        
    }


    public function storeuser(Request $request)


    {
    	

    	$validatedData = $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => 'required|email|max:255|unique:users,email',
            
        ]);

        $user= new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $test = uniqid().'_'.$user->id.'_'.'secreatshop';
        $user->slug = $test;
        $user->status = $request->status;
        $pass = uniqid();
        $user->password =Hash::make($pass);

                $username = $user->name;
                $useremail = $user->email;
                $userslug = $user->slug;
                $userpassword = $pass; 
          $data = array('userpassword'=>$pass,'username'=>$request->name,'useremail'=>$request->email,'userslug'=>$test);
          Mail::to($useremail)->send(new Validation($data));
        $user->save();
       

        session()->flash('status','insert');
        return redirect('/cd-admin/viewuser');
 
    }


    public function viewuser(Request $request)
    {
         if( Auth::user()->status == 'super-admin'){
        $user = User::where('id','!=',Auth::user()->id)->get();
        return view('cd-admin.user.userview',['user'=>$user]);
    }
    else{
    	return redirect('/cd-admin/home');
    }
        
        
        

    }


    public function changeStatus($id)
     {
        if( Auth::user()->status == 'super-admin'){
        try {
            $id = (int)$id;
            $user = user::find($id);
                if ($user->status == 'admin'){
                    $user->status = 'super-admin';
                    $user->save();
                    session()->flash('status','change');
                    return back();
                }else{
                $user->status = 'admin';
                $user->save();
               session()->flash('status','change');
                return back();
            }
            
        } catch (\Exception $e) {
            $message = $e->getMessage();
            session()->flash('error', $message);
            return back();
        }
        }
        else{
        	return redirect('/cd-admin/home');

        }

    }
   
    public function deleteuser($id)
    {
    	
        if( Auth::user()->status == 'super-admin'){
        $data = User::destroy($id);
        Session::flash('status','Deleted!!!');
        return redirect('/cd-admin/viewuser');
    }
         else{
    	return redirect('/cd-admin/home');
    }
         
    }


     public function validation($slug)
    {

        $user= User::where('slug',$slug)->get()->first();
         return view('cd-admin.user.userpassword',['user'=>$user]);
    }


     public function newpassword(Request $request,$id){

     	$getuser = User::findorfail($id);
        if (!(Hash::check($request->get('current-password'), $getuser->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $user = User::findorfail($id);
        $user->password = bcrypt($request->get('new-password'));
        $user->update();
         return redirect('/cd-admin');
    }

     
    
  public function forget()
    {

        
         return view('auth.forgetpassword');
    }



     public function changepassword(Request $request){

        $validatedData = $request->validate([
           
            'email' => 'required|email',
            
        ]);

        
        if(User::where('email', '=', Input::get('email'))->exists()){
        
         $user= User::where('email',$request->email)->get()->first();
        $name = $user->name;
        $slug = $user->slug;
        $email = $user->email;
      
        

               
                $username = $name;
                $useremail = $email;
                $userslug = $slug;
        $data = array('username'=>$username,'useremail'=>$useremail,'userslug'=>$userslug);
        Mail::to($useremail)->send(new PasswordFormMail($data));
        $user->save();
        session()->flash('status','insert');
        return redirect('/cd-admin');
    }
    else{
    	 session()->flash('status','fail');
        return redirect('/cd-admin');

    }
    
    }


     public function linkpassword($slug)
    {

        $user= User::where('slug',$slug)->get()->first();
         return view('cd-admin.user.linkpassword',['user'=>$user]);
    }



   public function newpasswordforget(Request $request,$id){

        $validatedData = $request->validate([
            'new-password' => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $user = User::findorfail($id);
        $user->password = bcrypt($request->get('new-password'));
        $user->update();
         return redirect('/cd-admin');
    }


   
}
