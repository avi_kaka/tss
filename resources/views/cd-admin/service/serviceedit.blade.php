@extends('cd-admin.home-master')

@section('page-title')
Service Edit
@endsection


@section('content')


<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Service Edit</div>
            
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{url('/cd-admin/updateservice',$getedits->id)}}" method="post" class="form-horizontal"  enctype="multipart/form-data">
                @csrf
                <div class="form-body">

                    <div class="form-group">
                            <label class="col-md-3 control-label"> Title</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa fa-user"></i>
                                    <input type="text" class="form-control input-circle" placeholder=" Title" name="title" value="{{$getedits->title }}" > </div>
                                    @if ($errors->has('title'))
                                                          <span class="help-block">{{ $errors->first('title') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>
                        
                    </div>
                     
                     <div class="form-group">
                            <label class="col-md-3 control-label"> Icon</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa fa-user"></i>
                                    <input type="text" class="form-control input-circle" placeholder=" Title" name="icon" value="{{$getedits->icon }}" > </div>
                                    @if ($errors->has('icon'))
                                                          <span class="help-block">{{ $errors->first('icon') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>
                        
          

                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}   margin-top-20">
                        <label class="control-label col-md-3"> shotr Description

                        </label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                 <textarea   name="description">{!!$getedits->description!!}</textarea>
                                @if($errors->has('description'))
                                <span class="text-danger font-weight-danger">
                                    {{$errors->first('description')}}
                                </span>

                                @endif
                            </div>
                        </div>
                    </div>

                      




                  
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="inputUserType3" class="col-md-3 control-label">Active</label>
                                <div class="col-md-4">
                                     <div class="md-radio">
                                    <input type="radio" id="radio14" name="status" value="1" {{$getedits->status==1 ? 'checked' : ''}}  class="md-radiobtn">
                                    <label for="radio14">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> Yes </label>
                                    </div>
                                     <div class="md-radio has-error">
                                        <input type="radio" id="radio15" name="status" value="0" {{$getedits->status==0 ?  'checked' : ''}}   class="md-radiobtn">
                                        <label for="radio15">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> NO </label>
                                        </div>
                                        @if ($errors->has('status'))
                                                          <span class="help-block">{{ $errors->first('status') }}</span>
                                                        @endif

                                </div>
                            </div>


                   
                        
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     <script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
                     <script >
                         tinymce.init({
                            selector:'textarea'
                         })
                     </script>


                    @endsection


