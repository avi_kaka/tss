<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contact_us;
use App\Mail\Contact;
use Mail;

class ContactController extends Controller
{
    public function store(Request $request)
    {
    	
    	$validatedData = $request->validate([
            'name' => 'required|min:3|max:255',
            'email' => 'required|email',
            'comment' => 'nullable',
            'phone_no' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10',
        ]);

        $contacts = new Contact_us;      		
        $contacts->name=$request->name;
        $contacts->email=$request->email;
        $contacts->phone_no=$request->phone_no;
        $contacts->description=$request->comment;


                
                $username = $contacts->name;
                $useremail = $contacts->email;

        $data = array('username'=>$username,'useremail'=>$useremail);
        Mail::to($useremail)->send(new Contact($data));
        $contacts->save();
        Session()->flash('success','Your Message is Successfully Sent!!');
        return redirect()->back();
    }
}
