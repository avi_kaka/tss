@extends('cd-admin.home-master')

@section('page-title')
Company Insert
@endsection


@section('content')


<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Company Insert</div>
            
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{url('/cd-admin/storecompany')}}" method="post" class="form-horizontal"  enctype="multipart/form-data">
                @csrf
                <div class="form-body">

                    <div class="form-group">
                            <label class="col-md-3 control-label"> Name</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa fa-user"></i>
                                    <input type="text" class="form-control input-circle" placeholder=" name" name="name" value="{{ old('name') }}" > </div>
                                    @if ($errors->has('name'))
                                                          <span class="help-block">{{ $errors->first('name') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>

                            <div class="form-group">
                            <label class="col-md-3 control-label"> Address</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa ab fa-facebook-f"></i>
                                    <input type="text" class="form-control input-circle" placeholder=" Address" name="address" value="{{ old('address') }}" > </div>
                                    @if ($errors->has('address'))
                                                          <span class="help-block">{{ $errors->first('address') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>



                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Phone Number</label>
                                                <div class="col-md-9">
                                                    <div class="mt-repeater">
                                                        <div data-repeater-list="test">
                                                            <div data-repeater-item class="row">
                                                                <div class="col-md-4">
                                                                    
                                                                    <input type="text" placeholder="Phone number" name="phone[]" class="form-control" /> </div>
                                                                
                                                                <div class="col-md-1">
                                                                    <label class="control-label">&nbsp;</label>
                                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                                        <i class="fa fa-close"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                            <i class="fa fa-plus"></i> Add Phone nuber</a>
                                                        <br>
                                                        <br> </div>
                                                </div>
                                            </div>





                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="inputUserType3" class="col-md-3 control-label">Active</label>
                                <div class="col-md-4">
                                     <div class="md-radio">
                                    <input type="radio" id="radio14" name="status" value="1" class="md-radiobtn">
                                    <label for="radio14">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> Yes </label>
                                    </div>
                                     <div class="md-radio has-error">
                                        <input type="radio" id="radio15" name="status" value="0"  class="md-radiobtn">
                                        <label for="radio15">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> NO </label>
                                        </div>
                                        @if ($errors->has('status'))
                                                          <span class="help-block">{{ $errors->first('status') }}</span>
                                                        @endif

                                </div>
                            </div>

                            <div class="form-group">
                            <label class="col-md-3 control-label">Facebook Link</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa ab fa-facebook-f"></i>
                                    <input type="text" class="form-control input-circle" placeholder="Facebook" name="facebook" value="{{ old('facebook') }}" > </div>
                                    @if ($errors->has('facebook'))
                                                          <span class="help-block">{{ $errors->first('facebook') }}</span>
                                                        @endif
                                </div>
                                
                            </div>

                            <div class="form-group">
                            <label class="col-md-3 control-label">Twitter Link</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa fa fa-twitter"></i>
                                    <input type="text" class="form-control input-circle" placeholder="Tweeter" name="tweeter" value="{{ old('tweeter') }}" > </div>
                                    @if ($errors->has('tweeter'))
                                                          <span class="help-block">{{ $errors->first('tweeter') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>

                             <div class="form-group">
                            <label class="col-md-3 control-label">Linkdin Link</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa fa fa-linkedin"></i>
                                    <input type="text" class="form-control input-circle" placeholder="Linkdin" name="link" value="{{ old('link') }}"  > </div>
                                    @if ($errors->has('link'))
                                                          <span class="help-block">{{ $errors->first('link') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>


                   
                        
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>


                    @endsection