<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use App\Fevicon;

class FeviconController extends Controller
{ 


	public function createfevicon(Request $request)
    {
        
        return view('cd-admin.fevicon.feviconinsert');
    }


    public function storefevicon(Request $request)


    {
    	

    	$validatedData = $request->validate([
			'alt_image'  => 'required',
			'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'


		]);
        
           $fdes = new Fevicon;
          if($request->hasFile('image'))
        {
            $file=Input::file('image');
            $destinationpath='uploads/fevicon';
            $uploadedImageName = uniqid().'_'.$file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath,$uploadedImageName);
            $fdes->image=$uploadedImageName;
        }
        $fdes->status= $request->status;
        $fdes->alt_image=$request->alt_image;
        $fdes->save();
       

        session()->flash('status','insert');
        return redirect('/cd-admin/viewfevicon');
 
    }


    public function viewfevicon(Request $request)
    {
        
        $test = Fevicon::all();
        return view('cd-admin.fevicon.feviconview',['test'=>$test]);
    }


    public function changeStatus($id)
     {
        try {
            $id = (int)$id;
            $fevicon = Fevicon::find($id);
                if ($fevicon->status == 0){
                    $fevicon->status = 1;
                    $fevicon->save();
                    session()->flash('status','change');
                    return back();
                }else{
                $fevicon->status = 0;
                $fevicon->save();
               session()->flash('status','change');
                return back();
            }
            
        } catch (\Exception $e) {
            $message = $e->getMessage();
            session()->flash('error', $message);
            return back();
        }

    }


    public function  editfevicon(Request $request,$id){
        $getedits = Fevicon::where('id',$id)->get()->first();
        return view('cd-admin.fevicon.feviconedit',['getedits'=>$getedits]);
    }




    public function updatefevicon(Request $request,$id)


    {
        

        $validatedData = $request->validate([
            'alt_image'  => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'


        ]);
        
           $getedits = Fevicon::findorfail($id);
          if($request->hasFile('image'))
        {

             $data_image = Fevicon::findOrFail($id);
            if ($data_image->image && file_exists("uploads/fevicon/{$data_image->image}")) {
                unlink("uploads/fevicon/{$data_image->image}");
            }
            $file=Input::file('image');
            $destinationpath='uploads/fevicon';
            $uploadedImageName = uniqid().'_'.$file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath,$uploadedImageName);
            $getedits->image=$uploadedImageName;
        }
        $getedits->status= $request->status;
        $getedits->alt_image=$request->alt_image;
        $getedits->update();
       

        session()->flash('status','success');
        return redirect('/cd-admin/viewfevicon');
 
    }
    


    public function deletefevicon($id) {
        // delete carousel image from folder
        $fevicon_image = Fevicon::findOrFail($id);
        if ($fevicon_image->image && file_exists("uploads/fevicon/{$fevicon_image->image}")) {
            unlink("uploads/fevicon/{$fevicon_image->image}");
        }
        $fevicon = fevicon::destroy($id);
        Session::flash('status','delet');
        return redirect('/cd-admin/viewfevicon');
    }
}
