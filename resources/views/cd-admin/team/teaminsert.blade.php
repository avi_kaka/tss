@extends('cd-admin.home-master')

@section('page-title')
Team Insert
@endsection


@section('content')


<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Team Insert</div>
            
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{url('/cd-admin/storeteam')}}" method="post" class="form-horizontal"  enctype="multipart/form-data">
                @csrf
                <div class="form-body">

                    <div class="form-group">
                            <label class="col-md-3 control-label"> Name</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa fa-user"></i>
                                    <input type="text" class="form-control input-circle" placeholder=" name" name="name" value="{{ old('name') }}" > </div>
                                    @if ($errors->has('name'))
                                                          <span class="help-block">{{ $errors->first('name') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>

                            <div class="form-group">
                            <label class="col-md-3 control-label"> Post</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa ab fa-facebook-f"></i>
                                    <input type="text" class="form-control input-circle" placeholder=" Post" name="post" value="{{ old('post') }}" > </div>
                                    @if ($errors->has('post'))
                                                          <span class="help-block">{{ $errors->first('post') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>



                                    <div class="form-group ">
                                                    <label class="control-label col-md-3">Image</label>
                                                    <div class="col-md-9">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>
                                                            <div>
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="image" value="{{ old('image') }}" accept="image/*"> </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>






                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="inputUserType3" class="col-md-3 control-label">Active</label>
                                <div class="col-md-4">
                                     <div class="md-radio">
                                    <input type="radio" id="radio14" name="status" value="1" class="md-radiobtn">
                                    <label for="radio14">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> Yes </label>
                                    </div>
                                     <div class="md-radio has-error">
                                        <input type="radio" id="radio15" name="status" value="0"  class="md-radiobtn">
                                        <label for="radio15">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> NO </label>
                                        </div>
                                        @if ($errors->has('status'))
                                                          <span class="help-block">{{ $errors->first('status') }}</span>
                                                        @endif

                                </div>
                            </div>

                            <div class="form-group">
                            <label class="col-md-3 control-label">Facebook Link</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa ab fa-facebook-f"></i>
                                    <input type="text" class="form-control input-circle" placeholder="Facebook" name="facebook" value="{{ old('facebook') }}" > </div>
                                    @if ($errors->has('facebook'))
                                                          <span class="help-block">{{ $errors->first('facebook') }}</span>
                                                        @endif
                                </div>
                                
                            </div>

                            <div class="form-group">
                            <label class="col-md-3 control-label">Twitter Link</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa fa fa-twitter"></i>
                                    <input type="text" class="form-control input-circle" placeholder="Tweeter" name="tweeter" value="{{ old('tweeter') }}" > </div>
                                    @if ($errors->has('tweeter'))
                                                          <span class="help-block">{{ $errors->first('tweeter') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>

                             <div class="form-group">
                            <label class="col-md-3 control-label">Linkdin Link</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa fa fa-linkedin"></i>
                                    <input type="text" class="form-control input-circle" placeholder="Linkdin" name="link" value="{{ old('link') }}"  > </div>
                                    @if ($errors->has('link'))
                                                          <span class="help-block">{{ $errors->first('link') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>


                   
                        
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>


                    @endsection