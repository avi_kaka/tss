@component('mail::message')

<h4>Hi, <strong>{{ $data['username'] }}!</strong></h4>
	You recently requested to reset your password for your  account. Use the button below to reset it.

@component('mail::button', ['url' => 'http://localhost/TSS/changepassword/'.$data['userslug']])

Reset Password
@endcomponent

 If you did not request a password reset, please ignore this email or <a href="#">contact support</a> if you have questions.

Thanks,<br>
{{ config('app.name') }}<br>
<img src="{{url('uploads/fevicon/'.$onefevicon->image)}}" width="100" height="100">
@endcomponent

    
