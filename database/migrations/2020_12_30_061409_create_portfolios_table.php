<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->string('title');
            $table->string('image')->nullable();
            $table->string('alt_image')->nullable();
            $table->string('slogan')->nullable();
            $table->date('date');
            $table->string('name');
            $table->string('category');
            $table->string('link')->nullable();;
            $table->text('description')->nullable();
             $table->integer('status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolios');
    }
}
