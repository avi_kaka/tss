<!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <!-- BEGIN SIDEBAR MENU -->
                        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <li class="sidebar-toggler-wrapper hide">
                                <div class="sidebar-toggler">
                                    <span></span>
                                </div>
                            </li>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                            <li class="sidebar-search-wrapper">
                                <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
                                <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
                                <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
                               <!--  <form class="sidebar-search  " action="page_general_search_3.html" method="POST">
                                    <a href="javascript:;" class="remove">
                                        <i class="icon-close"></i>
                                    </a>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search...">
                                        <span class="input-group-btn">
                                            <a href="javascript:;" class="btn submit">
                                                <i class="icon-magnifier"></i>
                                            </a>
                                        </span>
                                    </div>
                                </form> -->
                                <!-- END RESPONSIVE QUICK SEARCH FORM -->
                            </li>
                            <li class="nav-item start active open">
                                <a href="{{url('/cd-admin/home')}}" >
                                    <i class="icon-home"></i>
                                    <span class="title">Dashboard</span>
                                    <span class="selected"></span>
                                    <span class="arrow open"></span>
                                </a>
                                
                            </li>
                            @if( \Auth::user()->status == 'super-admin')

                             <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="icon-user"></i>
                                    <span class="title">User</span>
                                    <span class="arrow"></span>
                                </a>

                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{url('/cd-admin/insertuser')}}" class="nav-link ">
                                            <span class="title">Add New User</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="{{url('/cd-admin/viewuser')}}" class="nav-link ">
                                            <span class="title">View All User</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>
                              @endif
                             <li class="heading">
                                <h3 class="uppercase">-----------------------------------------</h3>
                            </li>

                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-list-alt"></i>
                                    <span class="title">Portfolio</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{url('/cd-admin/insertportfolio')}}" class="nav-link ">
                                            <span class="title">Add New Portfolio</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="{{url('/cd-admin/viewportfolio')}}" class="nav-link ">
                                            <span class="title">View All Portfolio</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>
                           


                             <li class="heading">
                                <h3 class="uppercase">-----------------------------------------</h3>
                            </li>

                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-tree-deciduous"></i>
                                    <span class="title">About</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{url('/cd-admin/insertabout')}}" class="nav-link ">
                                            <span class="title">Add New About</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="{{url('/cd-admin/viewabout')}}" class="nav-link ">
                                            <span class="title">View All About</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>
                            <li class="heading">
                                <h3 class="uppercase">-----------------------------------------</h3>
                            </li>

                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-bullhorn"></i>
                                    <span class="title">Services</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{url('/cd-admin/insertservice')}}" class="nav-link ">
                                            <span class="title">Add New Service</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="{{url('/cd-admin/viewservice')}}" class="nav-link ">
                                            <span class="title">View All Service</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>

                             <li class="heading">
                                <h3 class="uppercase">-----------------------------------------</h3>
                            </li>

                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="fa fas fa-users"></i>
                                    <span class="title">Teams</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{url('/cd-admin/insertteam')}}" class="nav-link ">
                                            <span class="title">Add New Team member</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="{{url('/cd-admin/viewteam')}}" class="nav-link ">
                                            <span class="title">View All Team Members</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>

                             <li class="heading">
                                <h3 class="uppercase">-----------------------------------------</h3>
                            </li>

                            <li class="nav-item  ">
                                <a href="javascript:;" class="nav-link nav-toggle">
                                    <i class="glyphicon glyphicon-picture"></i>
                                    <span class="title">Fevicon</span>
                                    <span class="arrow"></span>
                                </a>
                                <ul class="sub-menu">
                                    <li class="nav-item  ">
                                        <a href="{{url('/cd-admin/insertfevicon')}}" class="nav-link ">
                                            <span class="title">Add Fevicon</span>
                                        </a>
                                    </li>
                                    <li class="nav-item  ">
                                        <a href="{{url('/cd-admin/viewfevicon')}}" class="nav-link ">
                                            <span class="title">View Fevicon</span>
                                        </a>
                                    </li>
                                    
                                </ul>
                            </li>

                    


                           


                            
                            
                            
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->