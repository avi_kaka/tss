@extends('cd-admin.home-master')

@section('page-title')
About Edit
@endsection


@section('content')


<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>About Edit</div>
            
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{url('/cd-admin/updateabout',$getedits->id)}}" method="post" class="form-horizontal"  enctype="multipart/form-data">
                @csrf
                <div class="form-body">

                    <div class="form-group">
                            <label class="col-md-3 control-label"> Title</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa fa-user"></i>
                                    <input type="text" class="form-control input-circle" placeholder=" Title" name="title" value="{{$getedits->title }}" > </div>
                                    @if ($errors->has('title'))
                                                          <span class="help-block">{{ $errors->first('title') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>
                        
                    </div>
                     <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                                                    <label class="control-label col-md-3">Date</label>
                                                    <div class="col-md-3">
                                                        <div class="input-group input-medium date date-picker" data-date="12-02-2012" data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                                                            <input type="text" class="form-control" name="date" value="{{$getedits->date}}" readonly>
                                                            <span class="input-group-btn">
                                                                <button class="btn default" type="button">
                                                                    <i class="fa fa-calendar"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                        <!-- /input-group -->
                                                        <span class="help-block"> Select date </span>
                                                          @if($errors->has('date'))
                                <span class="text-danger font-weight-danger">
                                    {{$errors->first('date')}}
                                </span>

                                @endif
                                                    </div>
                                                </div>



                                    <div class="form-group ">
                                                    <label class="control-label col-md-3">Image</label>
                                                    <div class="col-md-9">
                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                            <div  class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"><img src="{{url('uploads/about/'.$getedits->image)}}"> </div>
                                                            <div>
                                                                <span class="btn red btn-outline btn-file">
                                                                    <span class="fileinput-new"> Select image </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" name="image" value="{{ $getedits->image }}" accept="image/*"> </span>
                                                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>


                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}   margin-top-20">
                        <label class="control-label col-md-3"> shotr Description

                        </label>
                        <div class="col-md-9">
                            <div class="input-icon right">
                                <i class="fa"></i>
                                 <textarea   name="description">{!!$getedits->description!!}</textarea>
                                @if($errors->has('description'))
                                <span class="text-danger font-weight-danger">
                                    {{$errors->first('description')}}
                                </span>

                                @endif
                            </div>
                        </div>
                    </div>

                      




                  
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="inputUserType3" class="col-md-3 control-label">Active</label>
                                <div class="col-md-4">
                                     <div class="md-radio">
                                    <input type="radio" id="radio14" name="status" value="1" {{$getedits->status==1 ? 'checked' : ''}}  class="md-radiobtn">
                                    <label for="radio14">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> Yes </label>
                                    </div>
                                     <div class="md-radio has-error">
                                        <input type="radio" id="radio15" name="status" value="0" {{$getedits->status==0 ?  'checked' : ''}}   class="md-radiobtn">
                                        <label for="radio15">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> NO </label>
                                        </div>
                                        @if ($errors->has('status'))
                                                          <span class="help-block">{{ $errors->first('status') }}</span>
                                                        @endif

                                </div>
                            </div>


                   
                        
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                     <script src="https://cdn.tiny.cloud/1/blvna7bbrjyv3rg786szzwomc5wijm95tylmftaxe7yppm5o/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
                     <script >
                         tinymce.init({
                            selector:'textarea'
                         })
                     </script>


                    @endsection


