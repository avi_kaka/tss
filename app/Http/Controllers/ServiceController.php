<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use App\Service;


class ServiceController extends Controller
{
    public function createservice(Request $request)
    {
        
        return view('cd-admin.service.serviceinsert');
    }


    public function storeservice(Request $request)


    {
    	

    	$validatedData = $request->validate([
			'title'  => 'required',
			'icon'  => 'required',
			'status'  => 'required',
			'description' => 'required|min:10',


		]);
        
           $fdes = new Service;
            $fdes->title= $request->title;; 
         $fdes->icon = $request->icon;
        $fdes->status= $request->status;
        $fdes->description=$request->description;

        // dd($fdes);
        $fdes->save();
       

        session()->flash('status','insert');
        return redirect('/cd-admin/viewservice');
 
    }


    public function viewservice(Request $request)
    {
        
        $service = Service::all();
        return view('cd-admin.service.serviceview',['service'=>$service]);
    }


    public function changeStatus($id)
     {
        try {
            $id = (int)$id;
            $service = Service::find($id);
                if ($service->status == 0){
                    $service->status = 1;
                    $service->save();
                    session()->flash('status','change');
                    return back();
                }else{
                $service->status = 0;
                $service->save();
               session()->flash('status','change');
                return back();
            }
            
        } catch (\Exception $e) {
            $message = $e->getMessage();
            session()->flash('error', $message);
            return back();
        }

    }


    public function  editservice(Request $request,$id){
        $getedits = Service::where('id',$id)->get()->first();
        return view('cd-admin.service.serviceedit',['getedits'=>$getedits]);
    }




    public function updateservice(Request $request,$id)


    {
        

        $validatedData = $request->validate([
			'title'  => 'required',
			'icon'  => 'required',
			'status'  => 'required',
			'description' => 'required|min:10',


		]);
        
          $getedits = Service::findorfail($id);
          $getedits->title= $request->title;; 
         $getedits->icon = $request->icon;
        $getedits->status= $request->status;
        $getedits->description=$request->description;
        $getedits->update();
       

        session()->flash('status','success');
        return redirect('/cd-admin/viewservice');
 
    }
    


    public function deleteservice($id) {
       
        $service = Service::destroy($id);
        Session::flash('status','delet');
        return redirect('/cd-admin/viewservice');
    }


}
