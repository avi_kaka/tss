<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Team;
use App\About;
use App\Service;
use App\Portfolio;

class FrontendController extends Controller
{
    public function index()
	{
		$team=Team::where('status',1)->get();
		$services=Service::where('status',1)->get();
		$portfolios=Portfolio::where('status',1)->get();
		$about=About::where('status',1)->orderBy('date','ASC')->get();
		return view('home.home',compact('team','about','services','portfolios'));

	}
}
