@extends('home-master')
@section('content')

    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
            <div class="container">
              
                <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="{{url('uploads/fevicon/'.$onefevicon->image)}}" alt="" /></a>

                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    Menu
                    <i class="fas fa-bars ml-1"></i>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav text-uppercase ml-auto">
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#services">Services</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#portfolio">Portfolio</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#about">About</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#team">Team</a></li>
                        <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Masthead-->
        <header class="masthead">
            <div class="container">
                <div class="masthead-subheading">Welcome To Our Studio!</div>
                <div class="masthead-heading text-uppercase">It's Nice To Meet You</div>
                <a class="btn btn-primary btn-xl text-uppercase js-scroll-trigger" href="#services">Tell Me More</a>
            </div>
        </header>
        <!-- Services-->
        <section class="page-section" id="services">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Services</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
                <div class="row text-center">
                    @foreach($services as $service)
                    <div class="col-md-4">
                        <span class="fa-stack fa-4x">
                            <i class="fas fa-circle fa-stack-2x text-primary"></i>
                            <i class="fas {{$service->icon}} fa-stack-1x fa-inverse"></i>
                        </span>
                        <h4 class="my-3">{{$service->title}}</h4>
                        <p class="text-muted">{!!$service->description!!}</p>
                    </div>
                    @endforeach
                    
                   
                </div>
            </div>
        </section>
        <!-- Portfolio Grid-->
        <section class="page-section bg-light" id="portfolio">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Portfolio</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
                <div class="row">
                    @foreach($portfolios as $port)
                    <div class="col-lg-4 col-sm-6 mb-4">
                        <div class="portfolio-item">
                            <a class="portfolio-link" data-toggle="modal" href="#check{{$port->id}}">
                                <div class="portfolio-hover">
                                    <div class="portfolio-hover-content"><i class="fas fa-plus fa-3x"></i></div>
                                </div>
                                <img class="img-fluid" src="{{url('uploads/portfolio/'.$port->image)}}" alt="{{$port->alt_image}}" />
                            </a>
                            <div class="portfolio-caption">
                                <div class="portfolio-caption-heading">{{$port->title}}</div>
                                <div class="portfolio-caption-subheading text-muted">{{$port->category}}</div>
                            </div>
                        </div>
                    </div>
                   
                  @endforeach
                   
                   
                   
                </div>
            </div>
        </section>
        <!-- About-->
        <section class="page-section" id="about">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">About</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
                <ul class="timeline">
                    <?php $isOdd = false?>
@foreach($about as $about)
<?php if($isOdd){
$isOdd = false;
}
else{
    $isOdd = true;
}

?>                 @if($isOdd)
                   
                    <li>
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="{{url('uploads/about/'.$about->image)}}" alt="{{$about->alt_img}}" /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                
                                <h4><?php
                                    $test = $about->date;
                                      echo date('F, Y', strtotime($test));
                                 ?></h4>
                                <h4 class="subheading">{{$about->title}}</h4>
                            </div>
                            <div class="timeline-body"><p class="text-muted">{!!$about->description!!}</p></div>
                        </div>
                    </li>
                    @else
                    
                    <li class="timeline-inverted">
                        <div class="timeline-image"><img class="rounded-circle img-fluid" src="{{url('uploads/about/'.$about->image)}}" alt="{{$about->alt_img}}" /></div>
                        <div class="timeline-panel">
                            <div class="timeline-heading">
                                <h4><?php
                                    $test = $about->date;
                                      echo date('F, Y', strtotime($test));
                                 ?></h4>
                                <h4 class="subheading">{{$about->title}}</h4>
                            </div>
                            <div class="timeline-body"><p class="text-muted">{!!$about->description!!}</p></div>
                        </div>
                    </li>
                  @endif
                  @endforeach
                    
                    <li class="timeline-inverted">
                        <div class="timeline-image">
                            <h4>
                                Be Part
                                <br />
                                Of Our
                                <br />
                                Story!
                            </h4>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <!-- Team-->
        <section class="page-section bg-light" id="team">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Our Amazing Team</h2>
                    <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3>
                </div>
                <div class="row">
                   @foreach($team as $team)
                    <div class="col-lg-3">
                        <div class="team-member">
                            @if(!empty($team->image))
                            <img class="mx-auto rounded-circle" src="{{url('uploads/team/'.$team->image)}}" alt="{{$team->alt_image}}" />
                          @else
                           <img class="mx-auto rounded-circle" src="{{url('uploads/default/god.jpg')}}" alt="gaming gear" />
                           @endif
                           
                            <h4>{{$team->name}}</h4>
                            <p class="text-muted">{{$team->post}}</p>
                            <a class="btn btn-dark btn-social mx-2" href="{{$team->tweeter}}" target="_blank"><i class="fab fa-twitter"></i></a>
                            <a class="btn btn-dark btn-social mx-2" href="{{$team->facebook}}" target="_blank"><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-dark btn-social mx-2" href="{{$team->linkin}}" target="_blank"><i class="fab fa-linkedin-in"></i></a>
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="row">
                    <div class="col-lg-8 mx-auto text-center"><p class="large text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut eaque, laboriosam veritatis, quos non quis ad perspiciatis, totam corporis ea, alias ut unde.</p></div>
                </div>
            </div>
        </section>
        <!-- Clients-->
        <div class="py-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-6 my-3">
                        <a href="#!"><img class="img-fluid d-block mx-auto" src="assets/img/logos/envato.jpg" alt="" /></a>
                    </div>
                    <div class="col-md-3 col-sm-6 my-3">
                        <a href="#!"><img class="img-fluid d-block mx-auto" src="assets/img/logos/designmodo.jpg" alt="" /></a>
                    </div>
                    <div class="col-md-3 col-sm-6 my-3">
                        <a href="#!"><img class="img-fluid d-block mx-auto" src="assets/img/logos/themeforest.jpg" alt="" /></a>
                    </div>
                    <div class="col-md-3 col-sm-6 my-3">
                        <a href="#!"><img class="img-fluid d-block mx-auto" src="assets/img/logos/creative-market.jpg" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact-->
        <section class="page-section" id="contact">
            <div class="container">
                <div class="text-center">
                    <h2 class="section-heading text-uppercase">Contact Us</h2>
                    <!-- <h3 class="section-subheading text-muted">Lorem ipsum dolor sit amet consectetur.</h3> -->
                </div>
                <form method="post" class="form-horizontal" action="{{url('frontend/store-contact')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <div class="row align-items-stretch mb-5">
                        <div class="col-md-6">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input class="form-control" id="name" type="text" placeholder="Your Name *" name="name"required="required" data-validation-required-message="Please enter your name." / >
                                @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input class="form-control" id="email" type="email" placeholder="Your Email *" name="email" required="required" data-validation-required-message="Please enter your email address." />
                                @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                <p class="help-block text-danger"></p>
                            </div>
                            <div class="form-group mb-md-0{{ $errors->has('phone_no') ? ' has-error' : '' }}">
                                <input class="form-control" id="phone" type="tel" placeholder="Your Phone *" name="phone_no" required="required" data-validation-required-message="Please enter your phone number." />
                                @if ($errors->has('phone_no'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('phone_no') }}</strong>
                                                </span>
                                            @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="col-md-6{{ $errors->has('comment') ? ' has-error' : '' }}">
                            <div class="form-group form-group-textarea mb-md-0">
                                <textarea class="form-control" id="message" placeholder="Your Message *" name="comment" data-validation-required-message="Please enter a message."></textarea>
                                @if ($errors->has('comment'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('comment') }}</strong>
                                                </span>
                                            @endif
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <div id="success"></div>
                        <button class="btn btn-primary btn-xl text-uppercase" id="sendMessageButton" type="submit">Send Message</button>
                    </div>
                </form>
            </div>
        </section>
        <!-- Footer-->
        <footer class="footer py-4">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 text-lg-left">Copyright © Your Website 2020</div>
                    <div class="col-lg-4 my-3 my-lg-0">
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                    <div class="col-lg-4 text-lg-right">
                        <a class="mr-3" href="#!">Privacy Policy</a>
                        <a href="#!">Terms of Use</a>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Portfolio Modals-->
        <!-- Modal 1-->
        @foreach($portfolios as $port)
        <div class="portfolio-modal modal fade" id="check{{$port->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- <div class="close-modal" data-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div> -->
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <div class="modal-body">
                                    <!-- Project Details Go Here-->
                                    <h2 class="text-uppercase">{{$port->title}}</h2>
                                    <p class="item-intro text-muted">{{$port->slogan}}</p>
                                     @if(!empty($port->link))
                                   <a href="{{$port->link}}" target="_blank"> <img class="img-fluid d-block mx-auto" src="{{url('uploads/portfolio/'.$port->image)}}" width="100" height="100" alt="{{$port->alt_image}}" /></a>
                                   @else
                                   <a href="#" target="_blank"> <img class="img-fluid d-block mx-auto" src="{{url('uploads/portfolio/'.$port->image)}}" width="100" height="100" alt="{{$port->alt_image}}" /></a>
                                   @endif

                                    <p>{!!$port->description!!}</p>
                                    <ul class="list-inline">
                                        <li>Date: <?php
                                    $test = $port->date;
                                      echo date('F, Y', strtotime($test));
                                 ?></li>
                                        <li>Client: {{$port->name}}</li>
                                        <li>Category: {{$port->category}}</li>
                                    </ul>
                                    <button class="btn btn-primary" data-dismiss="modal" type="button">
                                        <i class="fas fa-times mr-1"></i>
                                        Close Project
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
        
        
      
    </body>
  @endsection

