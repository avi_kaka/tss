<!DOCTYPE html>
<html lang="en">
<head>

 <meta charset="utf-8" />
        <title>@yield('page-title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for statistics, charts, recent events and reports" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
         <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/datatables/datatables.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
          <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/morris/morris.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/jqvmap/jqvmap/jqvmap.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-summernote/summernote.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-modal/css/bootstrap-modal.css')}}" rel="stylesheet" type="text/css" />
      <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css" />
       <link href="{{url('public/cd-admin/creatu/global/plugins/fancybox/source/jquery.fancybox.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/css/jquery.fileupload.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css')}}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{url('public/cd-admin/creatu/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{url('public/cd-admin/creatu/layouts/layout/css/layout.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/layouts/layout/css/themes/darkblue.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{url('public/cd-admin/creatu/layouts/layout/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/css/style.css')}}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 
        <link rel="icon" type="image/x-icon" href="{{url('uploads/fevicon/'.$onefevicon->image)}}" />

  
    </head>
    <!-- END HEAD -->
  <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
    <div class="page-wrapper">

        @include('cd-admin.header.header')

        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                @include('cd-admin.header.sidebar')

            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">

                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">

                    @yield('content')

                </div>

                <!-- END CONTENT BODY -->

            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->

        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> {{date("Y")}} &copy; Manufacture
                <a target="_blank" href="http://creatudevelopers.com">Tss</a>
            </div>

            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>

        </div>
        <!-- END FOOTER -->
    </div>

</body>
<!-- BEGIN CORE PLUGINS -->
 <script src="{{url('public/cd-admin/creatu/global/plugins/jquery.min.js')}}" type="text/javascript"></script>

        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>

        <script src="{{url('public/cd-admin/creatu/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>

        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>

        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>

        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>

        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{url('public/cd-admin/creatu/global/plugins/moment.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>

        <script src="{{url('public/cd-admin/creatu/global/plugins/morris/morris.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/morris/raphael-min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/counterup/jquery.waypoints.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/amcharts/amcharts/amcharts.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/amcharts/amcharts/serial.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/amcharts/amcharts/pie.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/amcharts/amcharts/radar.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/amcharts/amcharts/themes/light.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/amcharts/amcharts/themes/patterns.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/amcharts/amcharts/themes/chalk.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/amcharts/ammap/ammap.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/amcharts/ammap/maps/js/worldLow.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/amcharts/amstockcharts/amstock.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/horizontal-timeline/horizontal-timeline.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/flot/jquery.flot.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/flot/jquery.flot.resize.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/flot/jquery.flot.categories.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery.sparkline.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jqvmap/jqvmap/jquery.vmap.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js')}}" type="text/javascript"></script>

        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/scripts/datatable.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/datatables/datatables.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js')}}" type="text/javascript"></script>

        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-summernote/summernote.min.js')}}" type="text/javascript"></script>

        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-repeater/jquery.repeater.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-modal/js/bootstrap-modal.js')}}" type="text/javascript"></script>
         <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}" type="text/javascript"></script>


        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{url('public/cd-admin/creatu/global/scripts/app.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/pages/scripts/components-editors.min.js')}}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{url('public/cd-admin/creatu/pages/scripts/dashboard.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/pages/scripts/table-datatables-managed.min.js')}}" type="text/javascript"></script>
         <script src="{{url('public/cd-admin/creatu/pages/scripts/form-repeater.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/pages/scripts/components-date-time-pickers.min.js')}}" type="text/javascript"></script>
        
        
        <script src="{{url('public/cd-admin/creatu/pages/scripts/ui-extended-modals.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/pages/scripts/ui-sweetalert.min.js')}}" type="text/javascript"></script>

         <script src="{{url('public/cd-admin/creatu/pages/scripts/form-fileupload.min.js')}}" type="text/javascript"></script>


         <script src="{{url('public/cd-admin/creatu/global/plugins/fancybox/source/jquery.fancybox.pack.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/js/vendor/load-image.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/js/jquery.fileupload.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js')}}" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{url('public/cd-admin/creatu/layouts/layout/scripts/layout.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/layouts/layout/scripts/demo.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/layouts/global/scripts/quick-nav.min.js')}}" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>

         <script src="{{url('public/cd-admin/js/js.js')}}" type="text/javascript"></script>

         <script>
      $(document).ready(function() {
      $('#summernote').summernote();

        });
    </script>
    <script>
      $(document).ready(function() {
      $('#summernote1').summernote();

        });
    </script>

  
</html>