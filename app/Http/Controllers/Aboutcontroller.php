<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use App\About;
use Carbon\Carbon;

class Aboutcontroller extends Controller
{
     public function createabout(Request $request)
    {
        
        return view('cd-admin.about.aboutinsert');
    }


    public function storeabout(Request $request)


    {
    	
    	$validatedData = $request->validate([
			'title'  => 'required',
			'date'  => 'required',
			'status'  => 'required',
			'image.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'


		]);
        
           $fdes = new About;
            $fdes->title= $request->title;
            
         $test = str_replace('/', '-', $request->date);
          $newDate = date("Y-m-d", strtotime($test));
         $fdes->date = $newDate;
        
          if($request->hasFile('image'))
        {
            $file=Input::file('image');
            $destinationpath='uploads/about';
            $uploadedImageName = uniqid().'_'.$file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath,$uploadedImageName);
            $fdes->image=$uploadedImageName;
        }
        $fdes->status= $request->status;
        $fdes->alt_image=$request->title;
        $fdes->description=$request->description;
        $fdes->save();
       

        session()->flash('status','insert');
        return redirect('/cd-admin/viewabout');
 
    }


    public function viewabout(Request $request)
    {
        
        $about = About::all();
        return view('cd-admin.about.aboutview',['about'=>$about]);
    }


    public function changeStatus($id)
     {
        try {
            $id = (int)$id;
            $team = About::find($id);
                if ($team->status == 0){
                    $team->status = 1;
                    $team->save();
                    session()->flash('status','change');
                    return back();
                }else{
                $team->status = 0;
                $team->save();
               session()->flash('status','change');
                return back();
            }
            
        } catch (\Exception $e) {
            $message = $e->getMessage();
            session()->flash('error', $message);
            return back();
        }

    }


    public function  editabout(Request $request,$id){
        $getedits = About::where('id',$id)->get()->first();
        return view('cd-admin.about.aboutedit',['getedits'=>$getedits]);
    }




    public function updateabout(Request $request,$id)


    {
        

        $validatedData = $request->validate([
          'title'  => 'required',
			'date'  => 'required',
			'status'  => 'required',
			'image.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'


        ]);
        
           $getedits = About::findorfail($id);
          $getedits->title= $request->title;
            
         $test = str_replace('/', '-', $request->date);
          $newDate = date("Y-m-d", strtotime($test));
         $getedits->date = $newDate;
          if($request->hasFile('image'))
        {

             $data_image = About::findOrFail($id);
            if ($data_image->image && file_exists("uploads/about/{$data_image->image}")) {
                unlink("uploads/about/{$data_image->image}");
            }
            $file=Input::file('image');
            $destinationpath='uploads/about';
            $uploadedImageName = uniqid().'_'.$file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath,$uploadedImageName);
            $getedits->image=$uploadedImageName;
        }
         $getedits->status= $request->status;
        $getedits->alt_image=$request->title;
        $getedits->description=$request->description;
        $getedits->update();
       

        session()->flash('status','success');
        return redirect('/cd-admin/viewabout');
 
    }
    


    public function deleteabout($id) {
        // delete carousel image from folder
        $team_image = About::findOrFail($id);
        if ($team_image->image && file_exists("uploads/about/{$team_image->image}")) {
            unlink("uploads/about/{$team_image->image}");
        }
        $team = About::destroy($id);
        Session::flash('status','delet');
        return redirect('/cd-admin/viewabout');
    }




}
