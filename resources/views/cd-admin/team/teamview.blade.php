@extends('cd-admin.home-master')

@section('page-title')
View All Team
@endsection


@section('content')
@if (session('status')=='success')
               <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Updated" data-message="Member Updated Succesfuly" data-type="success" data-allow-outside-click="true" data-confirm-button-class="btn-success"></button>
               @elseif (session('status')=='delet')
            <button class="btn btn-danger mt-sweetalert gk-btn-success"  id="gk-success" data-title="Delete" data-message="Member Deleted" data-type="error" data-allow-outside-click="true" data-confirm-button-class="btn-danger"></button>
             @elseif (session('status')=='insert')
            <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Inserted" data-message="Member Insert Succesfuly" data-type="success" data-allow-outside-click="true" data-confirm-button-class="btn-success"></button>
             @elseif (session('status')=='change')
            <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Updated" data-message="Member Status Updated" data-type="success" data-allow-outside-click="true" data-confirm-button-class="btn-success"></button>
         @endif

         <div class="page-bar">
                            <ul class="page-breadcrumb">
                                <li>
                                    <a href="{{url('cd-admin/home')}}">Dashboard</a>
                                    <i class="fa fa-circle"></i>
                                </li>
                               
                                <li>
                                    <span>Team</span>
                                </li>
                            </ul>
                            <div class="page-toolbar">
                                <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                    <i class="icon-calendar"></i>&nbsp;
                                    <span class="thin uppercase hidden-xs"></span>&nbsp;
                                    <i class="fa fa-angle-down"></i>
                                </div>
                            </div>
                        </div>

<div class="row">
                            <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                    <div class="portlet-title">
                                        <div class="caption font-dark">
                                            <i class="icon-settings font-dark"></i>
                                       

                                            <span class="caption-subject bold uppercase"> Team Table</span>
                                        </div>
                                       
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-toolbar">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="btn-group">
                                                        <a id="sample_editable_1_new" class="btn sbold green" href="{{url('cd-admin/insertteam')}}"> Add New
                                                            <i class="fa fa-plus"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                              
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                            <thead>
                                                <tr>
                                                   
                                                    <th> Name </th>
                                                    <th> Post </th>
                                                    <th> Image </th>
                                                    <th> Status </th>
                                                    <th> Actions </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($team as $value)
                                                <tr class="odd gradeX">
                                                   
                                                     <td> {{$value->name}} </td>
                                                      <td> {{$value->post}} </td>
                                                      @if(!empty($value->image))
                                                    <td><img src="{{url('uploads/team/'.$value->image)}}" width="100" height="100"></td>
                                                  @else
                                                  <td><img src="{{url('uploads/default/god.jpg')}}" width="100" height="100"></td>
                                                  @endif
                                                      <td> @if($value->status)  <a href="{{ url('cd-admin/changeTeamStatus/' . $value->id) }}"> <span class="label label-sm label-success"> Active </span> </a> @else
                                                         <a href="{{ url('cd-admin/changeTeamStatus/' . $value->id) }}"><span class="label label-sm label-danger">Inactive</span> </a>@endif
                                                    </td>
                                                    
                                                  
                                                   
                                                     <td>
                                                        <div class="btn-group">
                                                            <button class="btn btn-xs green dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false"> Actions
                                                                <i class="fa fa-angle-down"></i>
                                                            </button>
                                                            <ul class="dropdown-menu pull-left" role="menu">
                                                                
                                                                <li>
                                                                    <a href="{{url("/cd-admin/teamedit/{$value->id}")}}">
                                                                        <i class="icon-tag"></i> Edit </a>
                                                                </li>
                                                                <li>
                                                                    
                                                                    <a class="btn red btn-outline sbold uppercase" data-target="#{{$value->id}}" data-toggle="modal"> Delete  </a>
                                                                </li>
                                                                
                                                                
                                                            </ul>
                                                        </div>
                                                    </td>
                                                </tr>



                                                @endforeach



                                                
                                            </tbody>
                                            
                                        </table>
                                    </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>
                        </div>

                         @foreach($team as $value)
                        <div id="ajax-modal" class="modal fade" tabindex="-1"> </div>
                         <div id="{{$value->id}}" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                                            <div class="modal-body">
                                                <p> Do You like To Delete It? </p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Cancel</button>
                                                <a href="{{url("/cd-admin/teamdeleteform/{$value->id}")}}"  class="btn red">Delete</a>
                                                
                                            </div>
                                        </div>



</object>
                                            </div>
                                            
                                            
                                            <div class="modal-footer">
                                                <button type="button" data-dismiss="modal" class="btn btn-outline dark">Close</button>
                                                
                                            </div>
                                        </div>
                                        @endforeach
                        @endsection