<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','frontend\FrontendController@index');
Route::get('home','frontend\FrontendController@index');

Route::get('forgetpassword','UserController@forget');
Route::post('changepassword','UserController@changepassword');
Route::get('validatepassword/{slug}','UserController@validation');
Route::post('/newpassword/{slug}','UserController@newpassword');
Route::get('changepassword/{slug}','UserController@linkpassword');
Route::post('/newpasswordforget/{slug}','UserController@newpasswordforget');

Route::group([
    'prefix'=>'frontend'
],function(){


Route::post('store-contact','ContactController@store');

});

//auth...........

Route::get('cd-admin', [
  'as' => 'login',
  'uses' => 'Auth\LoginController@showLoginForm'
]);
Route::post('cd-admin', [
  'as' => '',
  'uses' => 'Auth\LoginController@login'
]);
Route::post('logout', [
  'as' => 'logout',
  'uses' => 'Auth\LoginController@logout'
]);



Route::group([
    'prefix'=>'cd-admin',  'middleware' => 'auth'
],function(){

Route::get('/home', 'HomeController@index');



//Users.......
    Route::get('/insertuser','UserController@createuser');
    Route::post('/storeuser','UserController@storeuser');
    Route::get('/viewuser','UserController@viewuser');
    Route::get('/changeUserStatus/{id}', 'UserController@changeStatus');
    Route::get('/userdeleteform/{id}', 'UserController@deleteuser');


//Teams.......
    Route::get('/insertteam','Teamcontroller@createteam');
    Route::post('/storeteam','Teamcontroller@storeteam');
    Route::get('/viewteam','Teamcontroller@viewteam');
    Route::get('/changeTeamStatus/{id}', 'Teamcontroller@changeStatus');
    Route::get('/teamedit/{id}','Teamcontroller@editteam');
    Route::post('/updateteam/{id}','Teamcontroller@updateteam');
    Route::get('/teamdeleteform/{id}', 'Teamcontroller@deleteteam');


    //About.......
    Route::get('/insertabout','AboutController@createabout');
    Route::post('/storeabout','AboutController@storeabout');
    Route::get('/viewabout','AboutController@viewabout');
    Route::get('/changeAboutStatus/{id}', 'AboutController@changeStatus');
    Route::get('/aboutedit/{id}','AboutController@editabout');
    Route::post('/updateabout/{id}','AboutController@updateabout');
    Route::get('/aboutdeleteform/{id}', 'AboutController@deleteabout');

    //Service.......
    Route::get('/insertservice','ServiceController@createservice');
    Route::post('/storeservice','ServiceController@storeservice');
    Route::get('/viewservice','ServiceController@viewservice');
    Route::get('/changeServiceStatus/{id}', 'ServiceController@changeStatus');
    Route::get('/serviceedit/{id}','ServiceController@editservice');
    Route::post('/updateservice/{id}','ServiceController@updateservice');
    Route::get('/servicedeleteform/{id}', 'ServiceController@deleteservice');


    //Fevicon.......
    Route::get('/insertfevicon','FeviconController@createfevicon');
    Route::post('/storefevicon','FeviconController@storefevicon');
    Route::get('/viewfevicon','FeviconController@viewfevicon');
    Route::get('/changeFeviconStatus/{id}', 'FeviconController@changeStatus');
    Route::get('/feviconedit/{id}','FeviconController@editfevicon');
    Route::post('/updatefevicon/{id}','FeviconController@updatefevicon');
    Route::get('/fevicondeleteform/{id}', 'FeviconController@deletefevicon');


    //Portfolio.......
    Route::get('/insertportfolio','PortfolioController@createportfolio');
    Route::post('/storeportfolio','PortfolioController@storeportfolio');
    Route::get('/viewportfolio','PortfolioController@viewportfolio');
    Route::get('/changePortfolioStatus/{id}', 'PortfolioController@changeStatus');
    Route::get('/portfolioedit/{id}','PortfolioController@editportfolio');
    Route::post('/updateportfolio/{id}','PortfolioController@updateportfolio');
    Route::get('/portfoliodeleteform/{id}', 'PortfolioController@deleteportfolio');


    //Company.......
    Route::get('/insertcompany','CompanyController@createcompany');
    Route::post('/storecompany','CompanyController@storecompany');
    Route::get('/viewteam','CompanyController@viewteam');
    Route::get('/changeTeamStatus/{id}', 'CompanyController@changeStatus');
    Route::get('/teamedit/{id}','CompanyController@editteam');
    Route::post('/updateteam/{id}','CompanyController@updateteam');
    Route::get('/teamdeleteform/{id}', 'CompanyController@deleteteam');





});
