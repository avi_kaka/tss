<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Redirect;
use Session;
use App\Team;

class Teamcontroller extends Controller
{
     public function createteam(Request $request)
    {
        
        return view('cd-admin.team.teaminsert');
    }


    public function storeteam(Request $request)


    {
    	

    	$validatedData = $request->validate([
			'name'  => 'required',
			'post'  => 'required',
			'status'  => 'required',
			'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'


		]);
        
           $fdes = new Team;
            $fdes->name= $request->name;; 
         $fdes->post = $request->post;
          if($request->hasFile('image'))
        {
            $file=Input::file('image');
            $destinationpath='uploads/team';
            $uploadedImageName = uniqid().'_'.$file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath,$uploadedImageName);
            $fdes->image=$uploadedImageName;
        }
        $fdes->status= $request->status;
        $fdes->alt_image=$request->name;
        $fdes->facebook= $request->facebook;
        $fdes->tweeter= $request->tweeter;
        $fdes->linkin= $request->link;
        // dd($fdes);
        $fdes->save();
       

        session()->flash('status','insert');
        return redirect('/cd-admin/viewteam');
 
    }


    public function viewteam(Request $request)
    {
        
        $team = Team::all();
        return view('cd-admin.team.teamview',['team'=>$team]);
    }


    public function changeStatus($id)
     {
        try {
            $id = (int)$id;
            $team = Team::find($id);
                if ($team->status == 0){
                    $team->status = 1;
                    $team->save();
                    session()->flash('status','change');
                    return back();
                }else{
                $team->status = 0;
                $team->save();
               session()->flash('status','change');
                return back();
            }
            
        } catch (\Exception $e) {
            $message = $e->getMessage();
            session()->flash('error', $message);
            return back();
        }

    }


    public function  editteam(Request $request,$id){
        $getedits = Team::where('id',$id)->get()->first();
        return view('cd-admin.team.teamedit',['getedits'=>$getedits]);
    }




    public function updateteam(Request $request,$id)


    {
        

        $validatedData = $request->validate([
            'name'  => 'required',
            'post'  => 'required',
            'status'  => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'


        ]);
        
           $getedits = Team::findorfail($id);
            $getedits->name= $request->name;; 
         $getedits->post = $request->post;
          if($request->hasFile('image'))
        {

             $data_image = Team::findOrFail($id);
            if ($data_image->image && file_exists("uploads/team/{$data_image->image}")) {
                unlink("uploads/team/{$data_image->image}");
            }
            $file=Input::file('image');
            $destinationpath='uploads/team';
            $uploadedImageName = uniqid().'_'.$file->getClientOriginalName(); // unique file name with extension
            $file->move($destinationpath,$uploadedImageName);
            $getedits->image=$uploadedImageName;
        }
        $getedits->status= $request->status;
        $getedits->alt_image=$request->name;
        $getedits->facebook= $request->facebook;
        $getedits->tweeter= $request->tweeter;
        $getedits->linkin= $request->link;
        $getedits->update();
       

        session()->flash('status','success');
        return redirect('/cd-admin/viewteam');
 
    }
    


    public function deleteteam($id) {
        // delete carousel image from folder
        $team_image = Team::findOrFail($id);
        if ($team_image->image && file_exists("uploads/team/{$team_image->image}")) {
            unlink("uploads/team/{$team_image->image}");
        }
        $team = Team::destroy($id);
        Session::flash('status','delet');
        return redirect('/cd-admin/viewteam');
    }




}
