<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Tss | Admin</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #1 for " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{url('public/cd-admin/creatu/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/plugins/bootstrap-sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{url('public/cd-admin/creatu/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{url('public/cd-admin/creatu/global/css/plugins.min.css')}}" rel="stylesheet" type="text/css" />
        
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{{url('public/cd-admin/creatu/pages/css/login-2.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
        <link rel="icon" type="image/x-icon" href="{{url('uploads/fevicon/'.$onefevicon->image)}}" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            
                <img src="{{url('uploads/fevicon/'.$onefevicon->image)}}" style="height: 100px;" alt="{{$onefevicon->alt_image}}" /> </a>
        </div>
        <!-- END LOGO -->
        @if (session('status')=='fail')
               <button class="btn btn-danger mt-sweetalert gk-btn-success"  id="gk-success" data-title="Delete" data-message="Email doesnt match" data-type="error" data-allow-outside-click="true" data-confirm-button-class="btn-danger"></button>
            
             @elseif (session('status')=='insert')
            <button class="btn btn-success mt-sweetalert gk-btn-success" id="gk-success" data-title="Inserted" data-message="Please check your email" data-type="success" data-allow-outside-click="true" data-confirm-button-class="btn-success"></button>
            
         @endif
        <!-- BEGIN LOGIN -->
        <div class="content">
            <!-- BEGIN LOGIN FORM -->
            <form class="login-form" action="{{ route('login') }}" method="post">
                @csrf
                <div class="form-title">
                     <span class="form-title">Hello! let's get started</span>
                     <br>
                    <span class="form-subtitle">Sign in to continue.</span>
                </div>
                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any Email and password. </span>
                </div>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" />
                    
                    
                    @if ($errors->has('email'))
                                                <span class="alert alert-danger help-block">
                                                <span>  Email or password inccorect. </span>
                                            @endif
                                       

                                             </div>
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />
                     @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>Password didnt match</strong>
                                                </span>
                                            @endif

                     </div>
                <div class="form-actions">
                    <button type="submit" class="btn red btn-block uppercase">Login</button>
                    <div class="pull-right forget-password-block">
                        <a  href="{{url('forgetpassword')}}" id="forget-password" class="forget-password">Forgot Password?</a>
                    </div>
                </div>

               
              
                
                
            </form>


            <!-- END LOGIN FORM -->
            <!-- BEGIN FORGOT PASSWORD FORM -->
            
            
            <!-- END FORGOT PASSWORD FORM -->

            
        </div>
        <div class="page-footer">
            <div class="page-footer-inner" align="center"> {{date("Y")}} &copy; Manufacture
                <a target="_blank" href="#">TSS</a>
            </div>
        <!-- END LOGIN -->
        <!--[if lt IE 9]>
<script src="{{url('public/cd-admin/creatu/global/plugins/respond.min.js')}}"></script>
<script src="{{url('public/cd-admin/creatu/global/plugins/excanvas.min.js')}}"></script> 
<script src="{{url('public/cd-admin/creatu/global/plugins/ie8.fix.min.js')}}"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/global/plugins/bootstrap-sweetalert/sweetalert.min.js')}}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{url('public/cd-admin/creatu/global/scripts/app.min.js')}}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{url('public/cd-admin/creatu/pages/scripts/login.min.js')}}" type="text/javascript"></script>
        <script src="{{url('public/cd-admin/creatu/pages/scripts/ui-sweetalert.min.js')}}" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>

</html>s