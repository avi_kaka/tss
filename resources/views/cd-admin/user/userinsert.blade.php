@extends('cd-admin.home-master')

@section('page-title')
User Insert
@endsection


@section('content')


<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>User Insert</div>
            
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form action="{{url('/cd-admin/storeuser')}}" method="post" class="form-horizontal"  enctype="multipart/form-data">
                @csrf
                <div class="form-body">

                    <div class="form-group">
                            <label class="col-md-3 control-label"> Name</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="fa fa-user"></i>
                                    <input type="text" class="form-control input-circle" placeholder=" name" name="name" value="{{ old('name') }}" > </div>
                                    @if ($errors->has('name'))
                                                          <span class="help-block">{{ $errors->first('name') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>

                            <div class="form-group">
                            <label class="col-md-3 control-label"> Email</label>
                            <div class="col-md-4">
                                <div class="input-icon">
                                    <i class="envelope-o"></i>
                                    <input type="text" class="form-control input-circle" placeholder=" email" name="email" value="{{ old('email') }}" > </div>
                                    @if ($errors->has('email'))
                                                          <span class="help-block">{{ $errors->first('email') }}</span>
                                                        @endif
                                </div>
                                 
                            </div>









                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label for="inputUserType3" class="col-md-3 control-label">Role</label>
                                <div class="col-md-4">
                                     <div class="md-radio">
                                    <input type="radio" id="radio14" name="status" value="super-admin" class="md-radiobtn">
                                    <label for="radio14">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> Super-Admin </label>
                                    </div>
                                     <div class="md-radio ">
                                        <input type="radio" id="radio15" name="status" value="admin"  class="md-radiobtn">
                                        <label for="radio15">
                                            <span></span>
                                            <span class="check"></span>
                                            <span class="box"></span> Admin </label>
                                        </div>
                                        @if ($errors->has('status'))
                                                          <span class="help-block">{{ $errors->first('status') }}</span>
                                                        @endif

                                </div>
                            </div>



                   
                        
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-circle green">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>


                    @endsection